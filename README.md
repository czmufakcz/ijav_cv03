# Zadání úkolů

1. Set
    + Vytvořte abstraktní třídu User, který bude mít atributy:
        + enum ROLE - ADMIN, CUSTOMER, TRADER
        + int age
        + String name
    + Abstraktní třída bude mít konstruktor s parametrem age, name
    + Vytvořte třídy Admin, Customer, Trader
    + Vytvořte třídu UserFactory, která reprezentuje návrhový vzor Factory method
    + V metodě main, vytvořte dvě množiny
    + Do první množiny vložte:
        + ADMIN - age: 15, name: Josef
        + ADMIN - age: 27, name: Josef
        + ADMIN - age: 27, name: Michael
        + CUSTOMER - age: 30, name: Martin
        + CUSTOMER - age: 25, name: Zdenek
        + TRADER - age: 27, name: Karel
    + Do druhé množiny vložte:
        + ADMIN - age: 27, name: Josef
        + CUSTOMER - age:30, name: Adam
        + TRADER - age: 55, name: Jan
        + Přepište metody hashCode a equals, tak aby:
        + Nebyly duplicitní věky
    + Proveďte operaci sloučení a vypište
    + Proveďte operaci průnik a vypište
    + Přepište metody hashCode a equals, tak aby:
        + Nebyly duplicitní věky ani jméno
    + Proveďte operaci sloučení a vypište
    + Proveďte operaci průnik a vypište
2. Map
    + Vytvořte kolekci typu Map v metodě main
        + Klíč -  String
        + Hodnota - Integer
    + Načtěte “text k procvičení”
    + Projděte celý text a jednotlivá slova zaznamenávejte do kolekce, kde klíč tvoří slovo a hodnota tvoří počet výskytů.
    + Vypište kolekci



**Text k cvičení:**
Tým vědců Fakulty elektrotechniky a informatiky Univerzity Pardubice se podílí na prestižním projektu Evropské kosmické agentury (ESA). Výzkum má přispět k využití satelitní navigace pro lepší zabezpečení a řízení vlaků v Evropě.
Experti chtějí pro řízení provozu vlaků využít satelitní systém EGNOS, který se už dnes často používá k řízení letového provozu nad evropským kontinentem. Tím by zvýšili bezpečnost a pohodlí cestujících v rámci Evropské unie. A také provázali dvě rozsáhlé evropské infrastruktury - Evropský železniční systém řízení dopravy (ERTMS/ETCS) a satelitní systém EGNOS. Tuto strategii kromě ESA významně podporuje Úřad Evropské komise pro Globální družicový polohový systém (GNSS), Agentura pro evropský globální navigační družicový systém (GSA) v Praze, Evropská železniční agentura (ERA), Mezinárodní unie železnic (UIC) a další evropské i národní instituce a železniční průmysl.
„Naším cílem je bezpečně a co nejlevněji určit polohu vlaků a tuto informaci dál využívat pro jejich zabezpečení a řízení. V praxi to mimo jiné znamená nahradit nebo doplnit tzv. fyzické balízy, tedy prvky umístěné v kolejišti, které slouží k určení polohy vlaků, levnějšími virtuálními balízami. Ty jsou uložené v počítači na vlaku a detekované pomocí satelitní navigace,“ uvedl děkan Fakulty elektrotechniky a informatiky Univerzity Pardubice Ing. Zdeněk Němec, Ph.D.
Proto je hlavním cílem projektu posoudit, jak nejlépe i na železnici využít satelitní systém, který byl původně navržený pouze pro potřeby letectví. Je nutné, aby v případě zabezpečení vlaků splňoval velmi přísné požadavky na bezpečnost podle evropských norem. Dalším cílem vědců je navrhnout metodiku, která prokáže tuto bezpečnost a připravit k ní schvalovací proces použitelný ve všech členských státech Evropské unie.  „Očekáváme, že první výsledky projektu se prakticky uplatní při návrhu, realizaci a certifikaci systému řízení vlaků na trati Pinerolo-Sangone poblíž italského Turína zřejmě v příštím roce,“říká doc. Ing. Aleš Filip, CSc. z Fakulty elektrotechniky a informatiky Univerzity Pardubice. Výsledky a zkušenosti se využijí i pro plánování a realizaci podobných železničních systémů v České republice.
Využití satelitní navigace pro zabezpečení a řízení vlaků pomocí satelitů se vědci z Fakulty elektrotechniky a informatiky věnují více než 20 let. Kontakty s Evropskou kosmickou agenturou navázali už v rámci svého prvního mezinárodního projektu APOLO v letech 1998 - 2001. Tehdy se soustředili na testování nové technologie ESTB (EGNOS System Test Bed) pro zabezpečení vlaků. Na tuto práci navázalo mnoho národních a mezinárodních projektů, z nichž předposledním byl evropsko-americký projekt H2020 RHINOS (2016-2017) zaměřený na využití satelitní navigace pro zabezpečení vlaků. Fakulta elektrotechniky a informatiky ho řešila v konsorciu pod vedením RadioLabs (Itálie) a v úzké spolupráci s kolegy z kalifornské Stanfordovy univerzity. Kvalita jejich výsledků a dlouhodobé zkušenosti vedly k tomu, že se pardubičtí vědci podílí také na řešení nového projektu Evropské kosmické agentury.
Nový mezinárodní výzkumný projekt SYSTEM SUITABILITY STUDY FOR TRAIN POSITIONING USING GNSS IN ERTMS IN 2020 (Studie použitelnosti satelitní navigace (GNSS) pro evropský systém zabezpečení a řízení vlaků) odstartoval loni, když vědci Fakulty elektrotechniky a informatiky Univerzity Pardubice získali tendr s nabídkou ve spolupráci se společností Nottingham Scientific, Ltd. z Velké Británie. Dalšími členy konsorcia jsou společnosti Telespazio - VEGA (Velká Británie), GMV (Španělsko), TÜV Rheinland (Velká Británie) a kromě Univerzity Pardubice a její Fakulty elektrotechniky a informatiky se jí účastní subdodavatelé AŽD Praha a Výzkumný ústav železniční. Délka řešení projektu je 18 měsíců. 
doc. Ing. Aleš Filip, CSc. a Ing. Zdeněk Němec, Ph.D.
Fakulta elektrotechniky a informatiky UPa
