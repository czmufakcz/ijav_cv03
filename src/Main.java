import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // task1();
        task2();
    }

    public static void task1() {
        Set<User> set1 = new LinkedHashSet<>();
        Set<User> set2 = new LinkedHashSet<>();
        // SET 1
        set1.add(UserFactory.createUser(ROLE.ADMIN, 15, "Josef"));
        set1.add(UserFactory.createUser(ROLE.ADMIN, 27, "Josef"));
        set1.add(UserFactory.createUser(ROLE.ADMIN, 27, "Michael"));
        set1.add(UserFactory.createUser(ROLE.CUSTOMER, 30, "Martin"));
        set1.add(UserFactory.createUser(ROLE.CUSTOMER, 25, "Zdenek"));
        set1.add(UserFactory.createUser(ROLE.TRADER, 27, "Karel"));

        // SET 2
        set2.add(UserFactory.createUser(ROLE.ADMIN, 27, "Josef"));
        set2.add(UserFactory.createUser(ROLE.CUSTOMER, 30, "Adam"));
        set2.add(UserFactory.createUser(ROLE.TRADER, 55, "Jan"));

        System.out.println("---SET 1 without duplicate.");
        for (User user : set1) {
            System.out.println(user);
        }
        System.out.println("---SET 2 without duplicate.");
        for (User user : set2) {
            System.out.println(user);
        }

        System.out.println("---Merged set1 and set2.");
        Set<User> mergedSet = new LinkedHashSet<>(set1);
        mergedSet.addAll(set2);

        for (User user : mergedSet) {
            System.out.println(user);
        }

        System.out.println("---Inresection set1 and set2.");
        Set<User> intersectionSet = new LinkedHashSet<>(set1);
        intersectionSet.retainAll(set2);

        for (User user : intersectionSet) {
            System.out.println(user);
        }
    }

    public static void task2() {
        String text = "T�m v�dc� Fakulty elektrotechniky a informatiky Univerzity Pardubice se pod�l� na presti�n�m projektu Evropsk� kosmick� agentury (ESA). V�zkum m� p�isp�t k vyu�it� satelitn� navigace pro lep�� zabezpe�en� a ��zen� vlak� v Evrop�.\r\n" + "Experti cht�j� pro ��zen� provozu vlak� vyu��t satelitn� syst�m EGNOS, kter� se u� dnes �asto pou��v� k ��zen� letov�ho provozu nad evropsk�m kontinentem. T�m by zv��ili bezpe�nost a pohodl� cestuj�c�ch v r�mci Evropsk� unie. A tak� prov�zali dv� rozs�hl� evropsk� infrastruktury - Evropsk� �elezni�n� syst�m ��zen� dopravy (ERTMS/ETCS) a satelitn� syst�m EGNOS. Tuto strategii krom� ESA v�znamn� podporuje ��ad Evropsk� komise pro Glob�ln� dru�icov� polohov� syst�m (GNSS), Agentura pro evropsk� glob�ln� naviga�n� dru�icov� syst�m (GSA) v Praze, Evropsk� �elezni�n� agentura (ERA), Mezin�rodn� unie �eleznic (UIC) a dal�� evropsk� i n�rodn� instituce a �elezni�n� pr�mysl.\r\n" + "�Na��m c�lem je bezpe�n� a co nejlevn�ji ur�it polohu vlak� a tuto informaci d�l vyu��vat pro jejich zabezpe�en� a ��zen�. V praxi to mimo jin� znamen� nahradit nebo doplnit tzv. fyzick� bal�zy, tedy prvky um�st�n� v koleji�ti, kter� slou�� k ur�en� polohy vlak�, levn�j��mi virtu�ln�mi bal�zami. Ty jsou ulo�en� v po��ta�i na vlaku a detekovan� pomoc� satelitn� navigace,� uvedl d�kan Fakulty elektrotechniky a informatiky Univerzity Pardubice Ing. Zden�k N�mec, Ph.D.\r\n" + "Proto je hlavn�m c�lem projektu posoudit, jak nejl�pe i na �eleznici vyu��t satelitn� syst�m, kter� byl p�vodn� navr�en� pouze pro pot�eby letectv�. Je nutn�, aby v p��pad� zabezpe�en� vlak� spl�oval velmi p��sn� po�adavky na bezpe�nost podle evropsk�ch norem. Dal��m c�lem v�dc� je navrhnout metodiku, kter� prok�e tuto bezpe�nost a p�ipravit k n� schvalovac� proces pou�iteln� ve v�ech �lensk�ch st�tech Evropsk� unie.  �O�ek�v�me, �e prvn� v�sledky projektu se prakticky uplatn� p�i n�vrhu, realizaci a certifikaci syst�mu ��zen� vlak� na trati Pinerolo-Sangone pobl� italsk�ho Tur�na z�ejm� v p��t�m roce,���k� doc. Ing. Ale� Filip, CSc. z Fakulty elektrotechniky a informatiky Univerzity Pardubice. V�sledky a zku�enosti se vyu�ij� i pro pl�nov�n� a realizaci podobn�ch �elezni�n�ch syst�m� v �esk� republice.\r\n" + "Vyu�it� satelitn� navigace pro zabezpe�en� a ��zen� vlak� pomoc� satelit� se v�dci z Fakulty elektrotechniky a informatiky v�nuj� v�ce ne� 20 let. Kontakty s Evropskou kosmickou agenturou nav�zali u� v r�mci sv�ho prvn�ho mezin�rodn�ho projektu APOLO v letech 1998 - 2001. Tehdy se soust�edili na testov�n� nov� technologie ESTB (EGNOS System Test Bed) pro zabezpe�en� vlak�. Na tuto pr�ci nav�zalo mnoho n�rodn�ch a mezin�rodn�ch projekt�, z nich� p�edposledn�m byl evropsko-americk� projekt H2020 RHINOS (2016-2017) zam��en� na vyu�it� satelitn� navigace pro zabezpe�en� vlak�. Fakulta elektrotechniky a informatiky ho �e�ila v konsorciu pod veden�m RadioLabs (It�lie) a v �zk� spolupr�ci s kolegy z kalifornsk� Stanfordovy univerzity. Kvalita jejich v�sledk� a dlouhodob� zku�enosti vedly k tomu, �e se pardubi�t� v�dci pod�l� tak� na �e�en� nov�ho projektu Evropsk� kosmick� agentury.\r\n" + "Nov� mezin�rodn� v�zkumn� projekt SYSTEM SUITABILITY STUDY FOR TRAIN POSITIONING USING GNSS IN ERTMS IN 2020 (Studie pou�itelnosti satelitn� navigace (GNSS) pro evropsk� syst�m zabezpe�en� a ��zen� vlak�) odstartoval loni, kdy� v�dci Fakulty elektrotechniky a informatiky Univerzity Pardubice z�skali tendr s nab�dkou ve spolupr�ci se spole�nost� Nottingham Scientific, Ltd. z Velk� Brit�nie. Dal��mi �leny konsorcia jsou spole�nosti Telespazio - VEGA (Velk� Brit�nie), GMV (�pan�lsko), T�V Rheinland (Velk� Brit�nie) a krom� Univerzity Pardubice a jej� Fakulty elektrotechniky a informatiky se j� ��astn� subdodavatel� A�D Praha a V�zkumn� �stav �elezni�n�. D�lka �e�en� projektu je 18 m�s�c�. \r\n" + "doc. Ing. Ale� Filip, CSc. a Ing. Zden�k N�mec, Ph.D.\r\n" + "Fakulta elektrotechniky a informatiky UPa\r\n" + "";
        String[] array = text.split(" ");
        Map<String, Integer> countWords = new LinkedHashMap<>();

        for (String string : array) {
            int value = 1;
            if (countWords.containsKey(string)) {
                value = countWords.get(string);
                value++;
            }
            countWords.put(string, value);

        }

        for (Map.Entry<String, Integer> entry : countWords.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();

            System.out.println(key + " -> " + value);
        }
        System.out.println("elektrotechniky -> " + countWords.get("elektrotechniky"));

    }

}
